Students will use general purpose visualization tools and some simple scripting
to explore different visualization techniques.

Hands-on exercise: 

Students will be able to discuss volumetric and surface visualization
capabilities of software tools used in the course and relate user interface
controls to the visualization concepts discussed in the lecture. Students will
learn about volume rendering through a hands-on tutorial that demonstrates
principles used in standard volume visualization tools (Fiji, Neutube,
Paraview) and tricks for maximizing interpretability of volumetric data.
Students will also experiment with interactive browsing and exploration of
tabulated experimental data.

Key Concepts Learned:

Students will understand how to produce basic interactive visualizations of
volume, graph, and tensor data using standard tools.

References:

Paraview: http://www.paraview.org/

Graph Visualization: http://www.graphviz.org/

Mayavi: http://docs.enthought.com/mayavi/mayavi/



